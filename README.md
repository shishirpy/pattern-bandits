# Sleeping Bandits

Sleeping experts and bandits were introduced in [Klienberg et. al.][1]

## Types of matrices
Availability matrix is an easy way to model availability of arms.
We will test the following types of matrices.

1. All ones matrix (this is the standard multi-armed bandit) problem.
1. K-active arms problem: In this there can be at max k-active arms
at any given time instance.
1. Complementary arms: In this at every time step the active arms are
different from the ones that were active in the previous time step. 

[1]: http://www.cs.cornell.edu/~rdk/papers/colt08.pdf

---
## Storing Results
The following things are needed for analysis of any kind of multi-armed 
bandit problem. For every simulation we will store the following data:
1. List of $\mu$ values in order. 
1. 


### For each experiment save the following table 


| Time Step | Arms Active | Arm Played | Reward |
|-----------|-------------|------------|--------|
| 1         | [1, 3, 6]   | 3          | 0      |
|2          |[4, 3]|4 |1 |
|3|[1, 2,4] | 2 |   1     |




### For each horizon

Things that define a row are:
1. Random Seed
1. Number of iterations
1. Matrix type (for k-sparse : we will use k-sparse_5)

| Time Horizon | Regret |
|--------------|--------|
|20|3|
|21|3|

---
## Thoughts

What the are things that affect the regret because of the avaialibity
matrix $A$. s