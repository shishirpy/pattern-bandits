class DataFiles:
    """
    All operations with respect to file names.
    """
    def __init__(self):
        pass


    def get_feature_name(self, fname, split_by):
        """
        Extract the main feature of the file name.

        Parameters
        -------------
        fname : str 
            name of the file with extension 

        
        split_by : str 
            string to split by. The main feature will be to the 
            right of this string in the fname. 
        """

        feature_part = fname.strip().split(split_by)[1]
        feature_name = feature_part.split('_.')[0]

        return feature_name

