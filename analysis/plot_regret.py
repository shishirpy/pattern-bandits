import os

import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd

from data_files import DataFiles

# from utilities.data_files import DataFiles


mpl.style.use('ggplot')
# mpl.style.use('seaborn-dark')
# mpl.style.use('classic')
# print(mpl.style.available)

class Analysis:
    """
    All the functions needed to perform analysis on the saved data.
    """
    FILE_DIR = os.path.dirname(__file__)
    data_path = FILE_DIR + "/../data/"

    def __init__(self):
        self.df = None
        
    def regret_plots(self, file_names = None, has_string = None):
        """
        Create the regret plots for all the 
        """
        f_dat = DataFiles()

        file_list = os.listdir(self.data_path)
        all_data = dict()
        if file_names is None:
            for fname in file_list:
                if has_string == None:
                    if '.csv' in fname:
                        df = pd.read_csv(self.data_path + fname)
                        all_data[fname] = df
                else:
                    if (has_string in fname) and ('.csv' in fname):
                        df = pd.read_csv(self.data_path + fname)
                        all_data[fname] = df
                    elif 'ALL-ACTIVE' in fname:
                        df = pd.read_csv(self.data_path + fname)
                        all_data[fname] = df
        
        # create new data frame with all the data.
        df_all_data = pd.DataFrame(index = all_data[list(all_data.keys())[0]]['horizon'])
        for key in all_data:
            col_name = 'avg_regret_' + f_dat.get_feature_name(key, '_mat_type_')
            df_all_data[col_name] = all_data[key]['avg_regret']

        df_all_data.reindex(all_data[key]['horizon'])

        # print([i for i in all_data[key]['horizon']])
        # print([i for i in df_all_data.index])
        # print(df_all_data.columns)

        # make horizon the index
        # df_all_data.reindex('horizon')
        # print(df_all_data.set_index('horizon', inplace=True).describe())
        # df_all_data.plot(kind = 'line', xticks = df_all_data.index[::50], \
        # figsize = (10, 6))
        # plt.show()


        # create another plot with only better column names
        # cols = df_all_ data.columns
        # new_cols = [i.split('regret_')[-1] for i in df_all_data.columns]

        df_new = pd.DataFrame(index=df_all_data.index)
        for i, col in enumerate(df_all_data.columns):
            new_col = col.split('regret_')[-1]
            if 'ALTERNA' in new_col:
                new_col = 'ALT' + new_col[9:]
            df_new[new_col] = df_all_data[col]


        self.df = df_new

        # print(df_new)

        df_new.plot(kind='line', xticks=df_all_data.index[::50], \
        figsize=(10, 6), linewidth=1)
        plt.title("Regerts for different time horizon")
        plt.show()


    def ratio_plots(self):
        """
        Pass
        """
        df_ratio = pd.DataFrame(index=self.df.index)

        # find ratio between columns
        for col in self.df.columns:
            df_ratio[col] = self.df[col]/self.df['ALL-ACTIVE_K_None']

        # df.reindex(self.df.index)
        df_ratio.plot(kind='line', figsize=(10, 6), linewidth=1)
        plt.ylim(-1, 3.5)
        plt.title("Regret Ratio")
        plt.show()



if __name__ == "__main__":
    an = Analysis()
    an.regret_plots(has_string='_K_7')
    an.ratio_plots()
    # print(an.df.columns)
