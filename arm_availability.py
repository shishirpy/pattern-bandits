"""
author : shishir
"""

import numpy as np
from scipy import linalg


class ArmAvailability():
    """
    This class will have different functions for

    Parameters
    ----------------
    n_arms = int
        Number of arms

    T : int
        Time horizon


    Attributes
    --------------
    A : n_arms x T
        binary matrix where each column t tells the availability of
        each arm at time t.
    """
    matrix_types = ['RANDOM', 'ONE-ACTIVE', 'ALL-ACTIVE', 'K-SPARSE', \
    'ALTERNATE-K-SPARSE']

    def __init__(self, n_arms, T, matrix_type):
        self.n_arms = n_arms
        self.time_horizon = T
        self.availability_matrix = None
        if matrix_type not in self.matrix_types:
            raise ValueError("Matrix type not defined. Use of the \
            following: {0}".format(self.matrix_types))
        else:
            self.matrix_type = matrix_type


    def __gen_random(self):
        """
        Generate a completely random matrix.
        """
        self.availability_matrix = np.random.randint(0, 2, (self.n_arms, self.time_horizon))
        # return self.availability_matrix

    def __gen_one_active_arm(self):
        """
        Generate a matrix in which exactly one arm in active at any given
        time.

        This kind of availability matrix should have regret = 0.
        """
        # pick an arm at random every time and create the matrix
        active_arms = np.random.randint(0, self.n_arms, self.time_horizon)
        self.availability_matrix = np.zeros((self.n_arms, self.time_horizon))

        for i, arm in enumerate(active_arms):
            self.availability_matrix[arm][i] = 1

        # return self.availability_matrix

    def __gen_all_active_arms(self):
        """
        Generate matrix where all arms are active at all times.
        This is the standard multi-armed bandit problem.
        """
        self.availability_matrix = np.ones((self.n_arms, self.time_horizon))

        # return self.availability_matrix

    def __gen_k_sparse_active_arms(self, k):
        """
        This function will generate the availability matrix such that all
        columns are k-sparse.

        Parameters
        --------------
        k : int
            sparsity of the column vector.
        """
        if k >= self.n_arms:
            raise ValueError("The number of active arms should be strictly \
            less than number of arms {0}".format(self.n_arms))


        matrix = np.zeros((self.n_arms, 1))
        n_arms = [i for i in range(self.n_arms)]
        for i in range(self.time_horizon):
            col_vector = np.zeros((self.n_arms, 1))
            n_items = np.random.randint(k, k+1)
            active_arms = np.random.permutation(n_arms)[:n_items]
            for arm in active_arms:
                col_vector[arm][0] = 1
            matrix = np.c_[matrix, col_vector]
            # print(matrix)

        self.availability_matrix = matrix[:, 1:]
        
    def __gen_k_sparse_active_arms_2(self, k):
        """
        This function is there to avoid multiple contatications in similar fn
        above.

        This function will generate the availability matrix such that all
        columns are k-sparse.

        Parameters
        --------------
        k : int
            sparsity of the column vector.
        """
        if k >= self.n_arms:
            raise ValueError("The number of active arms should be strictly \
            less than number of arms {0}".format(self.n_arms))


        matrix = np.zeros((self.n_arms, self.time_horizon))
        n_arms = [i for i in range(self.n_arms)]
        for i in range(self.time_horizon):
            # col_vector = np.zeros((self.n_arms, 1))
            col_vector = np.zeros(self.n_arms)
            n_items = np.random.randint(k, k+1)
            active_arms = np.random.permutation(n_arms)[:n_items]
            for arm in active_arms:
                col_vector[arm] = 1
            # matrix = np.c_[matrix, col_vector]
            matrix[:, i] += col_vector
            # print(matrix)

        self.availability_matrix = matrix
        

    def __gen_alternate_k_sparse(self, k=None):
        """
        Generate a k-sparse matrix where each consecutive time slots have
        no common active arms.

        Parameters
        ---------------
        k : int
            sparsity of each active set.

        """
        if k >= self.n_arms:
            raise ValueError("The number of active arms should be strictly \
            less than number of arms {0}".format(self.n_arms))

        matrix = np.zeros((self.n_arms, 1))
        n_arms = [i for i in range(self.n_arms)]
        old_active_arms = [-1 for i in range(self.n_arms)]

        for i in range(self.time_horizon):
            col_vector = np.zeros((self.n_arms, 1))
            complement_set = set(n_arms) - set(old_active_arms)
            n_items = np.random.randint(1, k+1)
            if n_items > len(complement_set):
                n_items = len(complement_set)

            active_arms = np.random.permutation(list(complement_set))[:n_items]
            old_active_arms = active_arms.copy()

            for arm in active_arms:
                col_vector[arm][0] = 1
            matrix = np.c_[matrix, col_vector]
            # print(matrix)

        self.availability_matrix = matrix[:, 1:]


    def __gen_circulant_matrix(self, seed_col, n_arms, time_horizon):
        """
        Create a circulant matrix for the given seed column vector
        """
        if len(seed_col) != n_arms:
            raise ValueError("Lenght of seed column must be equal \
            to number of arms")

        circulant_matrix = linalg.circulant(seed_col)

        n_tiles = (time_horizon // n_arms) + 1

        availability_matrix = circulant_matrix.copy()
        for i in range(1, n_tiles):
            availability_matrix = np.c_[availability_matrix, circulant_matrix]

        availability_matrix = availability_matrix[:, :time_horizon]


    def gen_matrix(self, k=None):
        """
        Generate the matrix of the given type.

        Parameters
        -------------
        matrix_type : str
            Matrix type to be used.
        """

        matrix_type = self.matrix_type

        if matrix_type not in self.matrix_types:
            raise ValueError("{0} matrix type not supported. Use one of the \
            following: {1}".format(matrix_type, self.matrix_types))


        if matrix_type == 'RANDOM':
            self.__gen_random()
        elif matrix_type == 'ONE-ACTIVE':
            self.__gen_one_active_arm()
        elif matrix_type == 'ALL-ACTIVE':
            self.__gen_all_active_arms()
        elif matrix_type == 'K-SPARSE':
            self.__gen_k_sparse_active_arms_2(k=k)
        elif matrix_type == 'ALTERNATE-K-SPARSE':
            self.__gen_alternate_k_sparse(k=k)

        return self.availability_matrix


if __name__ == "__main__":
    MATRIX_TYPE = 'K-SPARSE'
    aa = ArmAvailability(5, 10, matrix_type=MATRIX_TYPE)
    A = aa.gen_matrix(k = 3)
    print(A)
