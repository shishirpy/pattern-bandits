# -*- coding: utf-8 -*-
"""
author : Shishir Pandey
email : shishir.py@gmail.com


 This is an implementation of the Awake Upper Estimated Reward
(AUER). The algorithm is presented in the paper - 

1. Regret Bounds for Sleeping Experts and Bandits
   - Rober D. Kleinberg and Alexandru Niculescu-Mizil and 
     Yogeshwar Sharma 

"""

import math

import numpy as np

from bernoulli_arm import BernoulliArm
from arm_availability import ArmAvailability


def ind_max(x):
    m = max(x)
    return x.index(m)
    

class AUER():
    """
     This is an implementation of the Awake Upper Estimated Reward
     (AUER). The algorithm is presented in the paper - 

    1. Regret Bounds for Sleeping Experts and Bandits
       - Rober D. Kleinberg and Alexandru Niculescu-Mizil and 
         Yogeshwar Sharma



    To make things simple we will have the following ordering on expected 
    reward of 
    p1 >= p2 >= ... >= pK
    
    where p1, p2, ... , pK represent the expected reward for arms 
    a1, a2, ..., aK respectively.
    
    Hence, the best arm to be played at everytime step t will the arm 
    with the smallest index.
    
    This will help us calculate the regret easily but the algorithm itself
    does not make use of this ordering.
    
    Attributes
    ---------------------
    
    counts : list
        Has length equal to the number of arms. Contains the number of times
        each arm has been played.
        
    values : list
        sample mean for respective arms
        
    n_arms : int
        number of arms
        
    active_sets : list of lists
        arms active at time t
        
    best_arm_t = list
        optimal arm at time t
    """
    def __init__(self):
        self.counts = list()
        self.values = list()
        self.active_sets = list()
        self.n_arms = 0
        self.best_arm_t = list() # optimal arm at time t
        self.A = None
        return

    
    def initialize(self, n_arms, time_horizon, matrix_type, **kwargs):
        """
        Parameters
        ----------------
        n_arms : int 
            Number of arms

        time_horizon: int
            Time horizon of the experiment

        matrix_type: str
            Type of the matrix. One from the available list
        """
        self.counts = [0 for col in range(n_arms)] 
        self.values = [0.0 for col in range(n_arms)]
        self.n_arms = n_arms
        
        mat = ArmAvailability(n_arms, time_horizon, matrix_type = matrix_type)
        if matrix_type in  ['K-SPARSE', 'ALTERNATE-K-SPARSE']:
            if kwargs is not None:
                k = kwargs.get('k', None)
                if k is None:
                    raise ValueError("Please send a value for 'k'")
                else:
                    # print(k)
                    self.A = mat.gen_matrix(k = k)

        else:
            self.A = mat.gen_matrix()
        return


    def select_active_set(self, time):
        """
        An active set is selected based on the matrix A.

        Parameters
        --------------
        time : int
            time index of the play.
        """

        active_set = [i for i, val in enumerate(self.A[:, time]) if val != 0]
        self.active_sets.append(active_set)
        self.__best_arm()


        
        
    def __best_arm(self):
        """
        This function will return the best arm for the present active set.
        This is mainly used to calculate the regret of the algorithm.
        """
        self.best_arm_t.append(min(self.active_sets[-1]))
        return


    def select_arm(self):
        active_arms = self.active_sets[-1]
        

        for arm in active_arms:
            if self.counts[arm] == 0:
                return arm

        ucb_values = [0.0 for arm in range(len(self.counts))]
        total_counts = sum(self.counts)

        for arm in active_arms:
        #    print(active_arms, arm)
            bonus = math.sqrt((2 * math.log(total_counts))/float(self.counts[arm]))
            ucb_values[arm] = self.values[arm] + bonus
        #    print(ucb_values)
            
            
        max_value_arm = active_arms[ind_max([ucb_values[i] for i in active_arms])]        
        return max_value_arm


    def update(self, chosen_arm, reward):
        self.counts[chosen_arm] += 1
        n = self.counts[chosen_arm]

        value = self.values[chosen_arm]
        new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
        self.values[chosen_arm] = new_value
        return
