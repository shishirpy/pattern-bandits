# Locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8


alias ls='ls -G'
export CLICOLOR=1
export LSCOLORS=Gxfxcxdxbxegedabagacad
