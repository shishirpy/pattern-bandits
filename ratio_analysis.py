import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import os


FILE_DIR =os.path.dirname(__file__)


def ratio_analysis(file_pattern, time_horizon):
    """
    Parameters
    ---------------

    file_pattern : str
        pattern of files to open

    time_horizon : int
        time horizon
    """

    data_dict = FILE_DIR + '/data/'

    all_files = os.listdir(data_dict)

    regret_data_files = list()
    p_value_data_files = list()

    for fname in all_files:
        if '.csv' in fname:
            if file_pattern in fname:
                if 'p_values'
                regret_data_files.append(fname)
                p_value_data_files.append(fname)
