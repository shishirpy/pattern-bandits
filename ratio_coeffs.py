import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from utilities.regret_bounds import RegretBounds
import pandas as pd 
import os 

FILE_DIR = os.path.dirname(__file__)


class AnalyzeRatio:
    """
    Analyse the ratio of the regrets. Also calculate the upper bound
    and the lower bound.

    Attributes
    -------------
    """

    def __init__(self, p_values, time_horizon):
        """
        Parameters
        --------------
        p_values : list 
            p_values of the arms 

        time_horizon : int 
            time horizon 

        file_names : dict

        """
        self.p_values = p_values
        self.time_horizon = time_horizon
        self.ratio_ucb_regret = None
        self.ratio_regret = None
        self.classical_regret_df = None
        self.auer_regret_df = None

    def __load_data(self, file_names):
        df = pd.read_csv(file_name)


    def ratio_regrets(self):
        """
        Analyse the output
        """
        reg_bounds = RegretBounds(self.p_values)
        ucb_regret = reg_bounds.ucb_regret_upper(self.time_horizon)
        auer_regret = reg_bounds.auer_regret_upper(self.time_horizon)

        self.ratio_ucb_regret = auer_regret / ucb_regret

        self.ratio_regret = 1
        



if __name__ == "__main__":

    file_pattern = "test_algo1"

    ALL_DATA_FILES = os.listdir(FILE_DIR + '/data/')


    an_ration = AnalyzeRatio()
