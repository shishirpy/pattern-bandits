import os
import numpy as np
import pandas as pd

class StoreResults():
    """
    Store the results of simulations. The results will be saved
    as comma separated value. With the name of the file have the
    parameters of simulation. The saved format will be the 
    following table.


    

    """

    # def __init__(self, file_name):
    #     """

    #     """
    #     # raise NotImplementedError("Class not yet implemented.")
    #     pass


    def store_regret_horizons_data(self, data, file_name):
        """
        Store the horizons data in the following data frame format.mro

        | Time Horizon | Regret |
        |--------------|--------|
        |            20|       3|
        |            21|       3|

        Parameters
        -------------------
        data : dict
            The data should be of the following format:

            {
                'horizon' : [ __, __, __,],
                'avg_regret'  : [ __, __, __,]
            }

        """

        df = pd.DataFrame()

        df['horizon'] = data['horizon']
        df['avg_regret'] = data['avg_regret']


        df.to_csv('data/'+file_name, index=False)


    def store_p_values(self, data, file_name):
        """
        """

        f_name = 'data/' + file_name
        np.savetxt(f_name, data)

        