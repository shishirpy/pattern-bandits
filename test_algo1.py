"""
Test the algorithm for a single run until horizon.

The regret plot we have will be the cumulative regret.
"""

from auer import AUER
from bernoulli_arm import BernoulliArm
import random
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import time

from store_results import StoreResults


mpl.style.use('ggplot')

def test_algorithm(algo, arms, num_sims=1, horizon=1000):
    """
    Parameters
    ----------------
    algo : Algorithm to be used for the experiments

    arms : Arms to be used by the algorithm (mostly one type of arms)

    num_sims : int
        number of simulations to be done

    horizon : int
        time horizon for each simulation



    Returns
    -----------------
    sim_nums :
        number of simulation to perform

    times :

    chosen_arms: A list of chosen arms

    rewards : list of rewards received

    cumulative_rewards : total cumulative reward received.
    """
    chosen_arms = [0.0 for i in range(num_sims * horizon)]
    rewards = [0.0 for i in range(num_sims * horizon)]
    cumulative_rewards = [0.0 for i in range(num_sims * horizon)]
    sim_nums = [0.0 for i in range( num_sims * horizon)]
    times = [0.0 for i in range(num_sims * horizon)]


    for sim in range(1, num_sims+1):
        # algo.initialize(len(arms), horizon, matrix_type )

        active_sets = list()
        best_arm_t = list()
        algo_values = list()
        for t in range(1, horizon + 1):
            index = (sim -1) * horizon + t - 1

            algo.select_active_set(time=t-1)
            active_sets.append(algo.active_sets[-1])

            best_arm_t.append(algo.best_arm_t[-1])

            algo_values.append(algo.values)

            sim_nums[index] = sim
            times[index] = t

            chosen_arm = algo.select_arm()
            chosen_arms[index] = chosen_arm

            reward = arms[chosen_arms[index]].draw()
            rewards[index] = reward
#            print("reward %s" %reward)

            if t == 1:
                cumulative_rewards[index] = reward
            else:
                cumulative_rewards[index] = cumulative_rewards[index -1] +reward

            algo.update(chosen_arm, reward)

        data_dict = dict()
        data_dict = {'sim_nums' : sim_nums, 'times': times, \
        'chosen_arms': chosen_arms, 'rewards' : rewards, 'cumulative_rewards':\
        cumulative_rewards}
        data_dict['active_sets'] = active_sets
        data_dict['best_arm_t'] = best_arm_t
        data_dict['algo_values'] = algo_values

        return [sim_nums, times, chosen_arms, rewards, cumulative_rewards, active_sets, best_arm_t, algo_values]


if __name__ == "__main__":
    # horizons = range(50, 1000)
    # horizons = np.arange(50, 51)
    horizons = [100000]
    # avail_mat_type = 'ALTERNATE-K-SPARSE'
    avail_mat_type = 'K-SPARSE'
    # avail_mat_type = 'ALL-ACTIVE'
    # K = 10

    # ks = range(5, 15)
    ks = [15, 16, 17, 18, 19]
    N_SIMS = 1
    N_ARMS = 20


    for K in ks:
        print(K)
        regret_f_name = ["test_algo1_", "Regret", "n_arms", str(N_ARMS), "n_iters", str(horizons[0]), "mat_type", \
        avail_mat_type, "K", str(K), 'ATLEAST_k_k', '.csv']

        p_values_f_name = ["test_algo1","p_values", "n_arms", str(N_ARMS), "n_iters", str(horizons[0]), "mat_type", \
        avail_mat_type, "K", str(K),'ATLEAST_k_k', '.csv']


        start_time = time.time()
        horizon_regrets = list()
        for horizon in horizons:
            print(horizon),
            regrets = []
            for sim_num in range(N_SIMS):
                n_arms = N_ARMS

                random.seed(0)
                p_values = [random.random() for i in range(n_arms)]
                p_values = sorted(p_values, reverse=True)

                random.seed(time.time())

                arms = [BernoulliArm(p) for p in p_values]
                algo = AUER()
                algo.initialize(n_arms, time_horizon=horizon,\
                matrix_type=avail_mat_type, k=K)

                sim_nums, times, chosen_arms, rewards, cumulative_rewards, \
                active_sets, best_arm_t, \
                algo_values = test_algorithm(algo, arms, horizon=horizon)

                # save active_arms
                # f = open("active_arms_auer.txt", 'w')
                # for i, subset in enumerate(active_sets):
                #     s = str(i) + " " + str(subset) + "\n"
                #     f.write(s)
                # f.close()

                regret = 0
                BREAK = 0
                step_regrets = list()
                for i in range(len(best_arm_t)):
                    delta = p_values[best_arm_t[i]] - p_values[chosen_arms[i]]
                    if  delta < 0:
                        BREAK = 1
                        break
                    else:
                        regret += delta
                        step_regrets.append(regret)
                regrets.append(regret)

                if BREAK == 1:
                    break

            horizon_regrets = step_regrets

            # horizon_regrets.append(np.average(regrets))

        end_time = time.time()

        print("Total time taken = {0} seconds".format(end_time - start_time))

        data_dict = dict()
        data_dict['horizon'] = range(horizons[0])
        data_dict['avg_regret'] = horizon_regrets

        # store results
        regret_file_name = "_".join(regret_f_name)
        p_values_file_name = "_".join(p_values_f_name)

        sr = StoreResults()
        sr.store_regret_horizons_data(data_dict, file_name=regret_file_name)
        sr.store_p_values(p_values, file_name=p_values_file_name)


        # print(len(horizons)), print(len(horizon_regrets))
        # fig = plt.figure()
        # ax = fig.add_subplot(111)
        # ax.plot(range(horizons[0]), horizon_regrets)
        # ax.set_title("Regret growth for sleeping bandits (ucb1)")
        # ax.set_ylabel("Regret")
        # ax.set_xlabel("Time horizons")
        # plt.show()


