import os
import re
import json


class RecordExpDetails():
    """
    Record the experiment details. All experimental details will be
    saved in a json file.

    Attributes
    -------------
    params : dict
        dictionary of items to be saved.
    """

    def __init__(self):
        self.params = dict()

    def initialize(self, **kwargs):
        """
        TODO : write this
        """

    def save_data(self):
        """
        TODO : write this
        """
        pass
