import numpy as np
import matplotlib as mpl 
import matplotlib.pyplot as plt 
import pandas as pd

class RegretBounds():
    """
    Class for evaluating the regret bounds for different bandit problems.

    Attributes
    -------------

    pvalues : list 
        descending sorted expected reward list  

    """

    def __init__(self, pvalues):
        if len(pvalues) < 2:
            raise ValueError("Please add atleast two arms.")

        self.pvalues = sorted(pvalues, reverse = True)

        if self.pvalues[0] == self.pvalues[1]:
            raise ValueError("There should only one best arm.")


    def ucb_regret_upper(self, n):
        """
        Upper bound for the standard UCB1 from 
        
        1. Finite-time Analysis of the Multiarmed Bandit Problem 
            - Auer et. al. (2002)

        Parameters
        ----------------
        n : int 
            time horizon 

        """

        s1 = 0
        s2 = 0

        mu_max = self.pvalues[0]
        for p in self.pvalues[1:]:
            s1 += 1./(mu_max - p)
            s2 += mu_max - p

        s1 *= 8 * np.log(n)
        s2 *= (1 + (np.pi**2)/ 3)

        return s1 + s2 


    def auer_regret_upper(self, n):
        """
        Upper bound for the AUER algorithm from 

        1. Regret Bounds on Sleeping Experts and Bandits 
            - Kleinberg et. al. 

        Parameters
        ----------------
        n : int 
            time horizon 
        """

        s = 0

        for i in range(len(self.pvalues))[:-1]:
            s += 1./(self.pvalues[i] - self.pvalues[i+1])

        s *= 64 * np.log(n)

        return s


    def auer_regret_lower(self, n):
        """
        Lower bound fo the AUER algorithm from 

        1. Regret Bounds on Sleeping Experts and Bandits 
            - Kleinberg et. al. 

        Parameters
        ----------------
        n : int 
            time horizon 
        """
        raise NotImplementedError("AUER regret lower bound in not implemented.")


if __name__ == "__main__":
    # np.random.seed(0)
    p_values = np.random.rand(10)
    # print p_values
    rb = RegretBounds(p_values)
    # print(rb.auer_regret_upper(100))
    # print(rb.ucb_regret_upper(100))

    regret_diff = list()
    auer_regret = list()
    ucb_regret = list()

    df = pd.DataFrame(columns = ['auer', 'ucb'])
    for i, n in enumerate(range(20, 1000)):
        tmp_doc = dict()
        aue_reg = rb.auer_regret_upper(n)
        ucb_reg = rb.ucb_regret_upper(n)
        diff = aue_reg  - ucb_reg

        tmp_doc['auer'] = aue_reg
        tmp_doc['ucb'] = ucb_reg
        # tmp_doc['diff'] = diff 

        # print(i)
        # df.loc[i] = [aue_reg, ucb_reg]#, inplace = True)
        df.loc[i+20] = tmp_doc
        # df. = tmp_doc

        # print(aue_reg)
        # print(ucb_reg)
        regret_diff.append(aue_reg - ucb_reg)


    mpl.style.use('ggplot')
    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # ax.hist(regret_diff, normed = True)
    # plt.show()

    # print(df)
    df.index.name = 'Time Horizon'
    # df.columns.name = 'Theoretical Upper Bound'
    df.plot(kind = 'line')#, set_xlable = "Time Horizon")
    plt.ylabel('Upper Bound')
    plt.savefig('upper_bounds.png')

    plt.show()


            

